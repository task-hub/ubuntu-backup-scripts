#!/bin/bash
date=`date '+%F_%H-%M-%S'`
mysqldump --skip-lock-tables -u root --password='' mydb | gzip > /tmp/$date.sql.gz
rsync -e 'ssh -i /root/.ssh/backup_id' -a /tmp/$date.tar.gz /tmp/$date.sql.gz backups@backup.example.com:~/
rm /tmp/$date.tar.gz /tmp/$date.sql.gz