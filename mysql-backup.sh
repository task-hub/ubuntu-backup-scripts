#!/bin/bash
date=`date '+%F_%H-%M-%S'`
# mysqldump --all-databases --skip-lock-tables -u root --password='' | gzip > /tmp/$date.sql.backup.gz
mysqldump --all-databases --skip-lock-tables -u root -p | gzip > /tmp/$date.sql.backup.gz
tar -czf ./$date.sql.backup.tar.gz /tmp/$date.sql.backup.gz
rm /tmp/$date.sql.backup.gz